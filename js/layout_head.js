$(document).ready(function(){
// обработка мобильного меню
	$(".afl_navigation_menu_block-head").on("click",function(){
		if ($(window).width()<768) {
			if($(this).next("ul").is(':visible')){
				$(this).next("ul").hide();
				$(this).next("ul").find("ul").hide();
				return false;
			}
				if($("ul").is(":visible"))$("ul").hide();
				$(this).next("ul").show();
				$(this).next("ul").find("ul").show();	
				
			return false;
		}
	});

	$(".navbar-toggle").click(function(){
		$(this).toggleClass("active");
		if(!$(this).hasClass("search")){
			$(".afl_body .sidebar").toggleClass("in");
		}else{
			$(".search-btn").removeClass("active");
			$(this).removeClass("search");
			$(".form-horizontal.afl_search").removeClass("active");
			$(".darked-bg").removeClass('active');
			setTimeout(function(){
				$(".darked-bg").hide();
			}, 500);
		}
	});

	$(".search-btn").click(function(){
		$(this).addClass("active");
		$(".afl_body .sidebar").removeClass("in");
		$(".form-horizontal.afl_search").addClass("active");
		$(".ya-site-form__input-text").focus();
		$(".darked-bg").show();
		setTimeout(function(){
			$(".darked-bg").addClass('active');
		}, 1);
		if(!$(".navbar-toggle").hasClass('active')){
			$('.navbar-toggle').toggleClass("active search");
		}else $('.navbar-toggle').toggleClass("search");
	});
	$(".darked-bg").click(function(){
		$(".search-btn").removeClass("active");
		$(".navbar-toggle").removeClass("active search");
		$(".form-horizontal.afl_search").removeClass("active");
		$(".darked-bg").removeClass('active');
		setTimeout(function(){
			$(".darked-bg").hide();
		}, 500);
	});

// кнопка наверх
if ( ($(window).height() + 100) < $(document).height() ) {
    $('#top-link-block').removeClass('hidden').affix({
        // how far to scroll down before link "slides" into view
        offset: {top:100}
    });
}

// фикс для iPhone
$(window).on("orientationchange",function(){
  document.location.href=document.location.href;
});

});

$(document).ready(function(){
    $('.slider-links a').click(function(){
        $('.slider a:visible').fadeOut()
        $('#slider-' + $(this).data('slider')).fadeIn();
        return false;
    });
});

//Скрипт прокрутки страницы
  function up() {
    var top = Math.max(document.body.scrollTop,document.documentElement.scrollTop);
   if(top > 0) {
    window.scrollBy(0,((top+100)/-10));
    t = setTimeout('up()',20);
   } else clearTimeout(t);
   return false;
  }
 $(window).scroll(function(){
 	scroll = $(this).scrollTop();
 	if(scroll > 200)$('.top-btn').css('opacity',".5");else $('.top-btn').css('opacity',"0");
 	if($('body').height() - $('footer').height() - $(window).height() +45 < scroll)
 		$('.top-btn').css('top',($('body').height() - $('footer').height()) ).css('position','absolute');
 	else $('.top-btn').css('top','auto').css('position','fixed');
 })